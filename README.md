# Calculator Frontend

This project is the frontend for a calculator app built using React Native and Expo. It provides a simple and intuitive user interface for performing basic arithmetic operations.

## Features

- Basic arithmetic operations (addition, subtraction, multiplication, division).
- Responsive design for both mobile and web platforms.
- Utilizes React Native Elements for enhanced UI components.

## Prerequisites

Before you begin, ensure you have met the following requirements:
- Node.js installed on your machine.
- Expo CLI installed. You can install it by running `npm install -g expo-cli`.

## Installation

To install the necessary dependencies, follow these steps:

git clone https://gitlab.com/jenniffer.rufino.dev/codechallengefrontend.git
cd calculatorfrontend
npm install

## Running the Application

You can run the application on your local machine by executing one of the following commands:

npm run start       # Runs the app in the development mode.
npm run android     # Opens your app in an Android device or emulator.
npm run ios         # Opens your app on a connected iOS device or iOS simulator.
npm run web         # Runs the app in the web browser.

## Contributing

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (git checkout -b feature/AmazingFeature)
3. Commit your Changes (git commit -m 'Add some AmazingFeature')
4. Push to the Branch (git push origin feature/AmazingFeature)
5. Open a Pull Request

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Acknowledgments

- React Native
- Expo
- React Native Elements
- All contributors and supporters
