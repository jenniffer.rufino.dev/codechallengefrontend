import axios from 'axios';
import { PaintCalculationResult, WallData } from '../types/interface';

export const calculatePaint = async (walls: WallData[]): Promise<PaintCalculationResult> => {
  try {
    const response = await axios.post('http://192.168.1.7:3001/api/calculate-paint', { walls });
    return response.data; 
  } catch (error) {
    console.error('Erro ao buscar cálculo de tinta:', error);
    throw new Error('Falha ao calcular tinta');
  }
};


