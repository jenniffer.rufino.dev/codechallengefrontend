export interface WallInput {
    width: string;
    height: string;
    doors: string;
    windows: string;
  }
  
  export interface WallData {
    width: number;
    height: number;
    doors: number;
    windows: number;
  }
  
  export interface PaintCalculationResult {
    totalLiters: number;
    buckets: { [key: string]: number };
    message?: string;
  }
  