import React from 'react';
import { Text } from 'react-native-elements';
import { StyleSheet } from 'react-native';

const Header: React.FC = () => {
  return (
    <Text h3 style={styles.header}>Calculadora de Tinta</Text>
  );
};

const styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    marginBottom: 20,
  },
});

export default Header;
