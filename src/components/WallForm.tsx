import React, { useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { calculatePaint } from '../services/paintService';
import { WallData, WallInput } from '../types/interface';
import WallInputGroup from './WallInputGroup';
import Header from './Header';

const defaultWallInput: WallInput = { width: '', height: '', doors: '', windows: '' };

const WallForm: React.FC = () => {
  const [walls, setWalls] = useState<WallInput[]>([
    { ...defaultWallInput },
    { ...defaultWallInput },
    { ...defaultWallInput },
    { ...defaultWallInput }
  ]);

  const handleInputChange = (text: string, index: number, field: keyof WallInput) => {
    const newWalls = walls.map((wall, i) => i === index ? { ...wall, [field]: text } : wall);
    setWalls(newWalls);
  };

  const handleSubmit = async () => {
    if (walls.some(wall => !wall.width || !wall.height)) {
      Toast.show({
        type: 'error',
        text1: 'Erro de Validação',
        text2: 'Certifique-se de que todos os campos estão corretamente preenchidos.'
      });
      return;
    }

    try {
      const formattedWalls: WallData[] = walls.map(wall => ({
        width: parseFloat(wall.width),
        height: parseFloat(wall.height),
        doors: parseInt(wall.doors, 10),
        windows: parseInt(wall.windows, 10)
      }));

      const result = await calculatePaint(formattedWalls);

      if (result.message) {
        Toast.show({
          type: 'info',
          text1: 'Informação',
          text2: result.message
        });
      }

      if (result.totalLiters > 0) {
        Toast.show({
          type: 'success',
          text1: `Total de Litros: ${result.totalLiters.toFixed(2)}L`,
          text2: `Latas: ${Object.entries(result.buckets).map(([size, count]) => `${count}x ${size}`).join(', ')}`
        });
      } else if (result.totalLiters === 0) {
        Toast.show({
          type: 'info',
          text1: 'Informação',
          text2: 'Nenhuma tinta necessária para as dimensões e quantidade de portas/janelas fornecidas.'
        });
      }
    } catch (error) {
      Toast.show({
        type: 'error',
        text1: 'Erro',
        text2: 'Falha ao calcular a quantidade de tinta necessária. Verifique o servidor e a rede.'
      });
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Header />
      {walls.map((wall, index) => (
        <WallInputGroup
          key={index}
          wall={wall}
          index={index}
          handleInputChange={handleInputChange}
        />
      ))}
      <Button
        title="Calcular Tinta Necessária"
        onPress={handleSubmit}
        buttonStyle={styles.button}
      />
      <Toast />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 20,
    backgroundColor: '#f5f5f5',
  },
  button: {
    backgroundColor: '#2089dc',
  },
});

export default WallForm;
