import React from 'react';
import { View } from 'react-native';
import { Input, Text, Icon, Divider } from 'react-native-elements';
import { WallInput } from '../types/interface';

interface WallInputGroupProps {
  wall: WallInput;
  index: number;
  handleInputChange: (text: string, index: number, field: keyof WallInput) => void;
}

const WallInputGroup: React.FC<WallInputGroupProps> = ({ wall, index, handleInputChange }) => {
  return (
    <View style={{ marginBottom: 20 }}>
      <Text h4 style={{ marginBottom: 10 }}>Parede {index + 1}</Text>
      <Input
        placeholder="Largura (m)"
        keyboardType="numeric"
        leftIcon={<Icon name="straighten" type="material" />}
        onChangeText={text => handleInputChange(text, index, 'width')}
        value={wall.width}
      />
      <Input
        placeholder="Altura (m)"
        keyboardType="numeric"
        leftIcon={<Icon name="height" type="material" />}
        onChangeText={text => handleInputChange(text, index, 'height')}
        value={wall.height}
      />
      <Input
        placeholder="Portas"
        keyboardType="numeric"
        leftIcon={<Icon name="door" type="material-community" />}
        onChangeText={text => handleInputChange(text, index, 'doors')}
        value={wall.doors}
      />
      <Input
        placeholder="Janelas"
        keyboardType="numeric"
        leftIcon={<Icon name="window-open" type="material-community" />}
        onChangeText={text => handleInputChange(text, index, 'windows')}
        value={wall.windows}
      />
      {index < 3 && <Divider style={{ marginVertical: 20 }} />}
    </View>
  );
};

export default WallInputGroup;
