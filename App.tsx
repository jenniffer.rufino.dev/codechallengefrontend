import React from 'react';
import { Container } from './styles'; 
import WallForm from './src/components/WallForm';

const App: React.FC = () => (
  <Container>
    <WallForm />
  </Container>
);

export default App;
